#include <MCP342x.h>
#include <Wire.h>
#include <SimpleKalmanFilter.h>

#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define MQTT_SERVER "206.189.144.182"
#define MQTT_PORT 1883
#define MQTT_USER "pog9"
#define MQTT_PASS "@#UnoenPog9S"


WiFiClient client;
PubSubClient mqtt(client);


float R1 = 100000.0;
float R2 = 25000.0;

uint8_t address = 0x6A;
MCP342x adc = MCP342x(address);

float RPULL = 10200.0;

SimpleKalmanFilter simpleKalmanFilter(22.58, 22.58, 1);
SimpleKalmanFilter simpleKalmanFilterRT(15.40, 15.40, 0.001);

#define IDTEMP    99

void setup() {

  Serial.begin(9600);
  Wire.begin();
  MCP342x::generalCallReset();

  Wire.requestFrom(address, (uint8_t)1);
  if (!Wire.available()) {
    Serial.print("No device found at address ");
    Serial.println(address, HEX);
    while (1)
      ;
  }

  WiFi.begin("Pom_2.4G", "0819699010");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }


  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());


  mqtt.setServer(MQTT_SERVER, MQTT_PORT);
  mqtt.setCallback(callback);
  reconnect();

}

void loop() {

  if (!mqtt.connected()) {
    reconnect();
  }


  MCP342x::Config status;

  long value = 0;
  long value2 = 0;
  uint8_t err = adc.convertAndRead(MCP342x::channel1, MCP342x::oneShot,
                                   MCP342x::resolution18, MCP342x::gain1,
                                   1000, value, status);


  err = adc.convertAndRead(MCP342x::channel2, MCP342x::oneShot,
                           MCP342x::resolution18, MCP342x::gain1,
                           1000, value2, status);


  float newKalman = simpleKalmanFilter.updateEstimate(value);



  if (err) {
    Serial.print("Convert error: ");
    Serial.println(err);
  } else {


    float check_vIn2V = value2 * (2.0 / 131071.0);
    float check_vin5V = check_vIn2V / (R2 / (R1 + R2));

    float newKalman = simpleKalmanFilter.updateEstimate(value);

    float vIn2V = value * (2.0 / 131071.0);
    float vin5V = vIn2V / (R2 / (R1 + R2));


    float vRaw = RPULL / ((check_vin5V / vin5V  ) - 1);
    float rtRaw = vRaw / (1 - (vRaw / 50000.0));


    vIn2V = newKalman  * (2.0 / 131071.0);
    vin5V = vIn2V / (R2 / (R1 + R2));

    float  vKalman = RPULL / ((check_vin5V / vin5V  ) - 1);
    float rtKalman = vKalman / (1 - (vKalman / 50000.0));

    float rtKalmanCal = simpleKalmanFilterRT.updateEstimate(rtKalman);


    char data[300];
    String payload = "{ \"rtRaw\": " + String(rtRaw) +
                     " , \"tempID\": " + String(IDTEMP ) +
                     " , \"rtKalman\": " + String(rtKalmanCal ) +
                     " , \"analogRaw\": " + String(value ) +
                     " , \"analogKalman\": " + String(newKalman ) +

                     " , \"checkvin5V\": " + String(check_vin5V ) +

                     " }";


    Serial.println(payload);

    String strObjData;

    Serial.print("Pub msg: ");
    Serial.println(strObjData);
    mqtt.publish("trm/temp", payload.c_str());

    delay(1000);
    mqtt.loop();

  }
}


void reconnect() {

  while (!mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (mqtt.connect("trm001", MQTT_USER, MQTT_PASS)) {
      //      Serial.println("connected");

    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  
  
}
